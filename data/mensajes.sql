CREATE DATABASE IF NOT EXISTS mensajes
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

USE mensajes;

DROP TABLE IF EXISTS mensaje;
CREATE TABLE mensaje (
  id int AUTO_INCREMENT PRIMARY KEY,
  texto varchar(255) NULL
);

INSERT INTO mensaje (texto) VALUES
('Hola Amigos'),
('Este es el segundo registro'),
('Aqu� va el tercero'),
(NULL),
('El  4 lo deje vacio este es el quinto');
